package com.nykredit.kundeservice.main;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.UIManager;
import javax.swing.WindowConstants;

public class SplashScreen extends JDialog {
	private static final long serialVersionUID = 1L;

	private String program = "Adherence Analyse v2.0";
	private JLabel labelSplash = null;

	public static void main(String[] args) {
		try {
	        UIManager.setLookAndFeel(
	            UIManager.getSystemLookAndFeelClassName());
	    } catch (Exception e) {}
	    
		SplashScreen splash = new SplashScreen();
		splash.dispose();
	}
	
	public SplashScreen(){
		this.setSize(400, 150);
		this.setLocationRelativeTo(this.getRootPane());
		this.setUndecorated(true);
		this.setContentPane(getLabelSplash());
		this.setTitle(program);
		this.setVisible(true);
		
		Adherence snitch = new Adherence(program);
		snitch.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE );
	}
	
	private JLabel getLabelSplash(){
		labelSplash = new JLabel(program,JLabel.CENTER);
		labelSplash.setOpaque(true);
		labelSplash.setBackground(new Color(34,56,127));
		labelSplash.setForeground(Color.white);
		labelSplash.setFont(new Font("Verdana", Font.ITALIC, 25));
		return labelSplash;
	}
}