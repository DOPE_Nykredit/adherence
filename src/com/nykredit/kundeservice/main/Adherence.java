package com.nykredit.kundeservice.main;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Rectangle;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.print.PrinterException;
import java.sql.SQLException;
import java.text.MessageFormat;

import javax.swing.JComboBox;
import javax.swing.JDesktopPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import com.nykredit.kundeservice.swing.DateSelector;
import com.nykredit.kundeservice.swing.KSCheckBoxTree;
import com.nykredit.kundeservice.swing.NFrame;
import com.nykredit.kundeservice.swing.PredefinedSwingComponents;
import com.nykredit.kundeservice.system.cAdherence;
import com.nykredit.kundeservice.table.AdherenceTable;
import com.nykredit.kundeservice.util.SF;






public class Adherence extends NFrame {
	private static final long serialVersionUID = 1L;

	private String program = null;
	private AdherenceTable Table = null;
	private cAdherence oracle = new cAdherence();
	private JScrollPane SPAdherenceTable = null;
	private PredefinedSwingComponents dope = new PredefinedSwingComponents();
	private DateSelector date;
	//private DateSelector date = new DateSelector(10,10,210,80);
	private KSCheckBoxTree KSCBT = null;

/*	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 *	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX Class Constructor XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 *	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 */
	public Adherence(String prog) {
		program = prog;
		
		try {oracle.Connect();}
		catch (SQLException e) {System.err.println("Exception: " + e);}
		oracle.SetupSpy(program);
		KSCBT = new KSCheckBoxTree(oracle);
		
		this.setSize(672, 567);
		this.setContentPane(Pane());
		this.setTitle(program);
		this.setResizable(false);
		this.setLocationRelativeTo(this.getRootPane());
		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent we) {
				dope.PromtClosing(program, Adherence.this);
			}
		});
		this.setVisible(true);
	}
/*	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 *	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX Private Functions XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 *	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 */
	private JPanel Pane() {	
		JPanel Pane = new JPanel();
		Pane.setLayout(new BorderLayout());
		Pane.add(DPane(), BorderLayout.CENTER);
		return Pane;
	}
	private JDesktopPane DPane() {
		JDesktopPane mainPane = new JDesktopPane();
		mainPane.setOpaque(false);
		mainPane.add(GetDateSelector());
		KSCBT = new KSCheckBoxTree (oracle, 10, 90);
		mainPane.add(KSCBT);
		mainPane.add(dope.CopyRight("DOPE", 570, 515));
		mainPane.add(dope.Button("Hent",10,415,new SF(){public void func() {refresh();}}));
		mainPane.add(dope.Button("Forklaring",10,440,new SF(){public void func() {explane();}}));
		mainPane.add(dope.Button("Udskriv",10,465,new SF(){public void func() {udskriv();}}));
		mainPane.add(dope.Button("Luk program",10,490,new SF(){public void func() {luk();}}));
		mainPane.add(getBankTable());
		date.setEndDateEnabled(false);
		//date.setDays(false,false,false,false,false,false,false);
		date.setEvent(new SF() {
			public void func() {
				KSCBT.loadTree(date.getStart(),date.getStart());
			}
		});
		return mainPane;
	}
	
	private JScrollPane getBankTable() {
		Table = new AdherenceTable(oracle);
		SPAdherenceTable = new JScrollPane();
		SPAdherenceTable.setBounds(new Rectangle(228, 10, 430, 505));
		SPAdherenceTable.getViewport().setBackground(Color.white);
		SPAdherenceTable.setViewportView(Table);
		return SPAdherenceTable;
	}
	private void refresh() {
		oracle.setDate(date.getStart());
		oracle.setWhereClause(KSCBT.getSelectionWhereClause("AA.", "AT."));
		Table.Set_View();
	}
	
	private void explane() {
		new Forklaring(this);
	}
	private void udskriv() {
		try {
			MessageFormat headerFormat = new MessageFormat(program);
			MessageFormat footerFormat = new MessageFormat(date.getStart()+" - Side {0}");
			Table.print(JTable.PrintMode.FIT_WIDTH, headerFormat, footerFormat);
		} catch (PrinterException e) {e.printStackTrace();}
	}
	private void luk() {
		dope.PromtClosing(program, Adherence.this);
	}

	private DateSelector GetDateSelector(){
		date = new DateSelector(10,10,210,80);

		return date;
		//new DateSelector(10,10,210,80);
	}
}