package com.nykredit.kundeservice.main;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Rectangle;

import javax.swing.BorderFactory;
import javax.swing.JDesktopPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import com.nykredit.kundeservice.swing.NDialog;
import com.nykredit.kundeservice.swing.NFrame;
import com.nykredit.kundeservice.swing.PredefinedSwingComponents;




public class Forklaring extends NDialog {
	private static final long serialVersionUID = 1L;

	private PredefinedSwingComponents dope = new PredefinedSwingComponents();

/*	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 *	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX Class Constructor XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 *	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 */
	public Forklaring(NFrame parentFrame) {
		this.setSize(180, 200);
		this.setContentPane(Pane());
		this.setTitle("Forklaring");
		this.setResizable(false);
		this.setLocationRelativeTo(parentFrame);
		this.setModal(true);
		this.setVisible(true);
	}
/*	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 *	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX Private Functions XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 *	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
 */
	private JPanel Pane() {
		JDesktopPane mainPane = new JDesktopPane();
		mainPane.setOpaque(false);
		mainPane.add(dope.CopyRight("DOPE", 570, 490));

		mainPane.add(getLabel(10,10,"Logget p� efter m�detid",new Color(221,254,162)));
		mainPane.add(getLabel(10,30,"Logget ud midt i vagten",new Color(219,169,241)));
		mainPane.add(getLabel(10,50,"Logget af f�r Vagt slut",new Color(182,221,232)));
		mainPane.add(getLabel(10,70,"Intet registreret",new Color(255,175,175)));

		mainPane.add(getLabel(10, 93,"Kolonnen �Timer� er r�d, n�r",null));
		mainPane.add(getLabel(10,105,"den overstiger 10 minutter",null));
		mainPane.add(getLabel(10,125,"Kolonnen �Sum� er r�d, n�r",null));
		mainPane.add(getLabel(10,137,"den overstiger 15 minutter",null));

		JPanel Pane = new JPanel();
		Pane.setLayout(new BorderLayout());
		Pane.add(mainPane, BorderLayout.CENTER);
		return Pane;
	}
	
	private JLabel getLabel(int x,int y,String text,Color bg) {
		JLabel lab = new JLabel(text,SwingConstants.CENTER);
		lab.setBounds(new Rectangle(x, y, 153, 16));
		lab.setFont(new Font("Dialog", Font.PLAIN, 10));
		if (bg != null) {
			lab.setBackground(bg);
			lab.setOpaque(true);
			lab.setBorder(BorderFactory.createMatteBorder( 1,1,2,2, Color.black));
		}
		return lab;
	}
}
