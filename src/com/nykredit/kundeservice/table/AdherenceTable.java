package com.nykredit.kundeservice.table;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.ResultSet;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;

import com.nykredit.kundeservice.system.cAdherence;



public class AdherenceTable extends JTable implements MouseListener {
	private static final long serialVersionUID = 1L;

	private DefaultTableModel thisModel;
	private cAdherence oracle = null;

	/*	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
	 *	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX Initialize XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
	 *	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
	 */
	public AdherenceTable(cAdherence dwh){
		oracle = dwh;
		this.getTableHeader().setPreferredSize(new Dimension(this.getTableHeader().getWidth(),40));
		this.setGridColor(Color.white);
		JTableHeader anHeader = getTableHeader();
		anHeader.setForeground(new Color(255,255,255));
		anHeader.setBackground(new Color(0,0,128));
		anHeader.setOpaque(false);

		addMouseListener(this);
	}
	/*	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
	 *	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX Private Functions XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
	 *	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
	 */
	private void TableHeader() {
		thisModel= new DefaultTableModel();
		getTableHeader().setReorderingAllowed(false);

		thisModel.addColumn ((Object) "Team");
		thisModel.addColumn ((Object) "Initialer");
		thisModel.addColumn ((Object) "Start");
		thisModel.addColumn ((Object) "");
		thisModel.addColumn ((Object) "Stop");
		thisModel.addColumn ((Object) "Timer");
		thisModel.addColumn ((Object) "Sum");
		thisModel.addColumn ((Object) "");

		setColumnSelectionAllowed(false);
		setRowSelectionAllowed(false);
		setCellSelectionEnabled(true);

		setDefaultRenderer(Object.class, new TableRender());

		setModel (thisModel);
		this.setAutoResizeMode(5);


		TableColumn col = getColumnModel().getColumn(3);
		col.setPreferredWidth(0);
		for (int i = 0 ; i < 8 ; i++ )
			getColumnModel().getColumn(i).setResizable(false);
	}

	private void TableData(ResultSet rs) throws Exception {
		String forrige = " ";
		double off = 0.0, tot = 0.0;
		
		String[] L = new String[8];
		while(rs.next()) {
			for ( int i = 0 ; i < 6 ; i++ ) {
				if (i<2 | i == 3) L[i] = rs.getString(i+1);
				else{
					L[i] = (convertTimeTo(rs.getDouble(i+1)));
				}
			}
			if (rs.getString(2).equalsIgnoreCase(forrige)) {
				L[6] = "";
				L[7] = "";
			} else {
				off += rs.getDouble(7);
				tot += rs.getDouble(8);
				
				L[6] = convertTimeTo((rs.getDouble(7)));
			}
			Integer timer = Integer.parseInt(L[5].substring(3, 5));
			Integer sum = 0;
			if (!L[6].isEmpty()) {
				 sum = Integer.parseInt(L[6].substring(3, 5));	
			}
			System.out.println(sum);
			if (L[6].isEmpty() && timer != 0) {
				if (thisModel.getRowCount() == 0) {
					if (sum != 0 || !L[6].isEmpty()) {
						thisModel.addRow(L);
					}
				}else {
					thisModel.addRow(L);
						
				}
				
			}else if  (timer >=  10 || sum >= 15) {
				thisModel.addRow(L);
			}
		
			forrige = rs.getString(2);
			System.out.println();
		}
		thisModel.addRow(new Object[]{"Total","","","","","",convertTimeTo((off))});
	}
	/*	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
	 *	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX Public Functions XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
	 *	'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
	 */	
	public String convertTimeTo(double time){
		double g = (time *1000);
		int minutes = (int) ((g / (1000*60)) % 60);
		int hours   = (int) ((g / (1000*60*60)) % 24);
		String finalHours;
		if (hours < 10) {
			 finalHours = "0"+hours;
		}else {
			finalHours = ""+hours;
		}
		String finalMinutes;
		if (minutes < 10) {
			finalMinutes = "0"+minutes;
		}else {
			finalMinutes = ""+minutes;
		}
		return finalHours+":"+finalMinutes;
	}
	
	public void Set_View() {
		try {
			oracle.SS_spy();
			TableHeader();
			ResultSet rs = oracle.getData();
			TableData(rs);
			rs.close();
		} catch (Exception e) {
			System.err.println("Exception: " + e);
		}
	}
	public void Empty() {
		thisModel= new DefaultTableModel();
		setModel (thisModel);
	}
	@Override
	public void mousePressed(MouseEvent e){
		if (e.isPopupTrigger())
			doPop(e);
	}
	@Override
	public void mouseReleased(MouseEvent e){
		if (e.isPopupTrigger())
			doPop(e);
	}
	private void doPop(MouseEvent e){
		right_click_menu menu = new right_click_menu(this);
		menu.show(e.getComponent(), e.getX(), e.getY());
	}
	@Override
	public void mouseClicked(MouseEvent e) {}
	@Override
	public void mouseEntered(MouseEvent e) {}
	@Override
	public void mouseExited(MouseEvent e) {}

}
