package com.nykredit.kundeservice.table;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JTable;
import javax.swing.SwingConstants;

import com.nykredit.kundeservice.swing.NTableRenderer;
import com.nykredit.kundeservice.util.Date;

public class TableRender extends NTableRenderer{
	private static final long serialVersionUID = 1L;

	public Component getTableCellRendererComponent (JTable table,Object obj, boolean isSelected, boolean hasFocus, int row, int column) {
		Component cell = super.getTableCellRendererComponent(table, obj, isSelected, hasFocus, row, column);
		int val = 0, r = 110, g, b;
		Date dope = new Date();
      
		setHorizontalAlignment(SwingConstants.CENTER);
//		r = cell.getBackground().getRed();
		g = cell.getBackground().getGreen()-r;
		b = cell.getBackground().getBlue()-r;
		if (g < 0) g = 0;
		if (b < 0) b = 0;

		if (column > 1 & column < 5) {
			if("MIV".equalsIgnoreCase((String)table.getValueAt(row, 3))) cell.setBackground(new Color(219,169,241));		// Midt i vagt
			else if("SAV".equalsIgnoreCase((String)table.getValueAt(row, 3))) cell.setBackground(new Color(221,254,162));	// Start af vagt
			else if("EAV".equalsIgnoreCase((String)table.getValueAt(row, 3))) cell.setBackground(new Color(182,221,232));	// Ende af vagt
			else if("V�K".equalsIgnoreCase((String)table.getValueAt(row, 3))) cell.setBackground(new Color(255,175,175));	// V�k - kan ikke findes
			
		} else if((column == 5 | column == 6) && table.getValueAt(row, column) != "") {
			val = dope.toTimeS((String)table.getValueAt(row, column));
			if (column == 5 & val >= 10 | column == 6 & val >=15)
				cell.setBackground(new Color(255,g,b));
		} else if((column == 7) && table.getValueAt(row, 7) != "") {
				val = dope.toTimeS((String)table.getValueAt(row, 6));
				if (val > 20) {
					//cell.setBackground(new Color(255,g,b));
				}
		}
		if ("Total".equalsIgnoreCase((String)table.getValueAt(row, 0))) {
			cell.setBackground(new Color(0,0,128));
			cell.setForeground(new Color(255,255,255));
		}
		
		addMarking(cell,isSelected,new Color(0,0,128),80);

    	return cell;
	}
}