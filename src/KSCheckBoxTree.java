

import java.awt.Dimension;
import java.awt.Rectangle;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JScrollPane;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import com.jidesoft.swing.CheckBoxTree;
import com.nykredit.kundeservice.data.CTIRConnection;
/*
 * CRPE var her
 */
public class KSCheckBoxTree extends JScrollPane {

	private static final long serialVersionUID = 1L;
	private CheckBoxTree tree = new CheckBoxTree();
	private CTIRConnection oracle;
	private String  notInTeamGruppe = "'Stab'",
					DatoS = "TRUNC(SYSDATE)",
					DatoE = "TRUNC(SYSDATE)",
					Sql =		
	"	SELECT AA.TEAM_NAVN, BB.INITIALER, AA.TEAM			"+
	"	FROM KS_DRIFT.AGENTER_TEAMS AA						"+
	"	INNER JOIN KS_DRIFT.V_TEAM_DATO BB					"+
	"	ON AA.TEAM = BB.TEAM								"+
	"	AND BB.DATO >= " + DatoS + "						"+
	"	AND BB.DATO <= " + DatoE + "						"+
	"	AND AA.TEAM_GRUPPE NOT IN (" + notInTeamGruppe + ")	"+
	"	ORDER BY AA.TEAM_NAVN, BB.INITIALER					";
	
	public KSCheckBoxTree(CTIRConnection oracle,int left, int top){
		this.oracle = oracle;
		loadTree();
		this.setBounds(new Rectangle(left, top, 210, 325));
		this.setViewportView(tree);
	}
	
	public KSCheckBoxTree(CTIRConnection oracle,int left, int top,int height){
		this.oracle = oracle;
		loadTree();
		this.setBounds(new Rectangle(left, top, 210, height));
		this.setViewportView(tree);
	}
	
	public KSCheckBoxTree(CTIRConnection oracle){
		this.oracle = oracle;
		loadTree();
		this.setPreferredSize(new Dimension(210,325));
		this.setViewportView(tree);
	}
	
	public KSCheckBoxTree(CTIRConnection oracle,String startDate,String endDate){
		this.oracle = oracle;
		loadTree(startDate,endDate);
		this.setPreferredSize(new Dimension(210,325));
		this.setViewportView(tree);
	}
	
	/**henter tr�et baseret p� den satte sql og eller notInTeamGruppe variable*/
	public void loadTree(String startDate,String endDate){
		this.DatoS = "'"+startDate+"'";
		this.DatoE = "'"+endDate+"'";
		DefaultMutableTreeNode top = new DefaultMutableTreeNode("Kundeservice");
	    DefaultMutableTreeNode category = null;
	    DefaultMutableTreeNode book = null;
		try {
			ResultSet rs4 = oracle.Hent_tabel(Sql);
			String ged = "";
	        while(rs4.next()) {
	        	if (!ged.startsWith(rs4.getString("TEAM"))) {
	        		ged = rs4.getString("TEAM");
		            category = new DefaultMutableTreeNode(rs4.getString("TEAM_NAVN"));
		            top.add(category);
	        	}
	            book = new DefaultMutableTreeNode(rs4.getString("INITIALER"));
		        category.add(book);
			}
	        rs4.close();
		} catch (SQLException e1) {}
		DefaultTreeModel test = new DefaultTreeModel(top);
		tree.setModel(test);
	}
	
	public void loadTree(){
		DefaultMutableTreeNode top = new DefaultMutableTreeNode("Kundeservice");
	    DefaultMutableTreeNode category = null;
	    DefaultMutableTreeNode book = null;
		try {
			ResultSet rs4 = oracle.Hent_tabel(Sql);
			String ged = "";
	        while(rs4.next()) {
	        	if (!ged.startsWith(rs4.getString("TEAM"))) {
	        		ged = rs4.getString("TEAM");
		            category = new DefaultMutableTreeNode(rs4.getString("TEAM_NAVN"));
		            top.add(category);
	        	}
	            book = new DefaultMutableTreeNode(rs4.getString("INITIALER"));
		        category.add(book);
			}
	        rs4.close();
		} catch (SQLException e1) {}
		DefaultTreeModel test = new DefaultTreeModel(top);
		tree.setModel(test);
	}
	
	/**returnerer sql koden der bliver brugt til at danne listen KSCheckBoxTree*/
	public String getSql(){return Sql;}
	/**s�tter sql koden der bliver brugt til at danne listen KSCheckBoxTree*/
	public void setSql(String Sql){this.Sql=Sql;}
	/**returnerer teamgruppe(r) der ikke skal med i listen*/
	public String getNotInTeamGruppe(){return notInTeamGruppe;}
	/**s�tter teamgruppe(r) der ikke skal med i listen*/
	public void setNotInTeamGruppe(String notInTeamGruppe){this.notInTeamGruppe=notInTeamGruppe;}
	
	
	/**returnerer en where clause til en sql med valgte team og agenter
	 * hvis alt er valgt returnerer den "" hvis ingen ting er valgt returnerer den 1=2
	 */
	public String getSelectionWhereClause(String VT, String AT){
		
		String team ="'SuperTroopers'";
		String initialer ="'XXXX'";
		String whereClause = "";
		boolean writeClause = false;
		
		try{
			for (TreePath i: tree.getCheckBoxTreeSelectionModel().getSelectionPaths()) {
				if (i.getPathCount() == 1){
					//alt er valgt s� der skal ikke skrives noget i whereClause
					break;	
				}
				else if (i.getPathCount() == 2 ) {
					team = team + ",'" + i.getLastPathComponent() + "'";
					writeClause = true;
				}
				else if (i.getPathCount() == 3) {
					initialer = initialer + ",'" + i.getLastPathComponent() + "'";	
					writeClause = true;
				}	
			}
			if (writeClause){
				whereClause = "\n AND "+AT+"TEAM_GRUPPE NOT IN ("+notInTeamGruppe+") \n"+
				"AND ("+AT+"TEAM_NAVN IN ("+team+") OR "+VT+"INITIALER IN ("+initialer+")) \n ";
			}
		} catch(Exception e){
			whereClause = AT+"TEAM_GRUPPE NOT IN ("+notInTeamGruppe+") AND";
		}
		return whereClause;
	}

}
